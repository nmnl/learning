* hmily(个人学习)
* hmily-demo-tcc-springcloud(本文演示TCC事务模式)
```text
* tcc事务模式copy官网demo
* 技术栈
1. 配置模块:hmily.server.configMode: local / zookeeper / nacos / apollo / etcd / consul
2. 存储模式:repository: database(mysql) / file / mongo / zookeeper / redis
3. 监控指标:metrics: ..prometheus
4. 技术栈: spring-boot < 2.3.15 / eureka(测试中暂无限制) / feign(测试中暂无限制) / ribbon (测试中暂无限制)
5. 技术文档写的特别详细, 在此感谢dromara 开源组织(为往圣继绝学，一个人或许能走的更快，但一群人会走的更远。)
```
* [代码](https://gitee.com/dromara/hmily.git)
* [文档](https://dromara.org/zh/projects/hmily/config/)

* 代码整体架构(最近更新10个月前-未做新版spring boot.2.3.15之后兼容)
```text
hmily-all 
hmily-annotation
hmily-bom
hmily-common
hmily-config
hmily-core
hmily-demo ：所有demo,这是比seata写的更全的测试用例
hmily-metrics : 目前hmily的metrics模块，采用 prometheus来进行采集，使用pull模式对外暴露metrics信息接口。收集的metrics主要分为二个大类。应用的JVM信息：内存，cpu，线程使用等等事务信息：包括事务的总数，事务的迟延，事务的状态，事务的角色
hmily-repository: 存储库插件代码
hmily-rpc:
hmily-serializer:
hmily-spi:
hmily-spring-boot-starter:
hmily-spring:
hmily-tax:
hmily-tcc:
hmily-xa:
```
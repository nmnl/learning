# seata-tcc-dubbo-shadingsphere
# 特别声明:
```text
1. 代码来源参考(特别感谢): https://gitee.com/benwang6/seata-samples.git
2. TCC-dubbo以及完善说明文档
3. 仅供学习使用
4. 部署建议:理论上提供一台装有docker的linux系统,该示例就能正常运行.请替换192.168.10.145
```
# dubbo: Dubbo RPC
```java
/**
 * The type Transaction propagation filter.
 */
@Activate(group = { Constants.PROVIDER, Constants.CONSUMER }, order = 100)
public class TransactionPropagationFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionPropagationFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String xid = RootContext.getXID(); // 获取当前事务 XID
        String rpcXid = RpcContext.getContext().getAttachment(RootContext.KEY_XID); // 获取 RPC 调用传递过来的 XID
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("xid in RootContext[" + xid + "] xid in RpcContext[" + rpcXid + "]");
        }
        boolean bind = false;
        if (xid != null) { // Consumer：把 XID 置入 RPC 的 attachment 中
            RpcContext.getContext().setAttachment(RootContext.KEY_XID, xid);
        } else {
            if (rpcXid != null) { // Provider：把 RPC 调用传递来的 XID 绑定到当前运行时
                RootContext.bind(rpcXid);
                bind = true;
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("bind[" + rpcXid + "] to RootContext");
                }
            }
        }
        try {
            return invoker.invoke(invocation); // 业务方法的调用

        } finally {
            if (bind) { // Provider：调用完成后，对 XID 的清理
                String unbindXid = RootContext.unbind();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("unbind[" + unbindXid + "] from RootContext");
                }
                if (!rpcXid.equalsIgnoreCase(unbindXid)) {
                    LOGGER.warn("xid in change during RPC from " + rpcXid + " to " + unbindXid);
                    if (unbindXid != null) { // 调用过程有新的事务上下文开启，则不能清除
                        RootContext.bind(unbindXid);
                        LOGGER.warn("bind [" + unbindXid + "] back to RootContext");
                    }
                }
            }
        }
    }
}
```

## 准备环境

> 启动 Nacos server
* [Nacos Server 下载地址](https://github.com/alibaba/nacos/releases)
* [Nacos Server Docker](./nacos-docker/standalone-mysql-8.yaml)

> 启动Seata Server
* [Seata Server 下载地址](https://github.com/seata/seata/releases)
* [Seata Server Docker](./seata-docker/seata-server142-nacos210-mysql8.yaml)


## 使用组件介绍

* Nacos 注册中心:v2.1.0
* Nacos 配置中心:v2.1.0
* dubbo 服务调用RPC: 2.7.15
* Seata 分布式事务解决方案TCC: 1.4.2

## 项目目录介绍
- seata-tcc-dubbop-shadingsphere
    - dubbo-interface
    - dubbo-server-user-order
      `订单服务`
    - dubbo-service-storage
      `产品库存服务`
    - dubbo-service-account
      `账户服务`
    - seata-docker `seata-server 1.4.2 : docker-compose 配置信息`
    - nacos-docker `nacos-server v2.1.0 : docker-compose 配置信息`
    - sql
      `服务需要的sql脚本`

- 版本

```xml
<spring-cloud.version>Hoxton.SR9</spring-cloud.version>
<spring-cloud-alibaba.version>2.2.3.RELEASE</spring-cloud-alibaba.version>
<shardingsphere.version>4.1.1</shardingsphere.version>
<seata-spring-boot.version>1.4.2</seata-spring-boot.version>
<mybatis-sb.version>2.1.4</mybatis-sb.version>
```

## demo

> storage超限:(未处理幂等的问题(commit/cancel||rollback))
```
curl --location --request POST 'http://localhost:8900/create/localTCC' \
--header 'User-Agent: Apipost client Runtime/+https://www.apipost.cn/' \
--header 'Content-Type: application/json' \
--data '{
    "id":2022061501,
    "userId":1,
    "productId":1,
    "count":101,//超出
    "money":100
}'

- http://localhost:8900/create/localTCC
- http://service-account/decrease/{userId}/{money}
    `出现exception`
  - seata-server mysql 
    global_table 全局TM入口.
        service-user-order 异常;记录TM
    branch_table 各个RM入口
        service-user-order 执行cancel
        service-account 执行cancel
        service-storage 执行cancel
```




* 感谢seata团队的帮助。
* 感谢sharding团队的帮助。
* 感谢nacos团队的帮助。
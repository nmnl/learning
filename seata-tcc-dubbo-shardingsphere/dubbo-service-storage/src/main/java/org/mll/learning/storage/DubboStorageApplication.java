package org.mll.learning.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubboStorageApplication {
    public static void main(String[] args) {
        SpringApplication.run(DubboStorageApplication.class, args);
    }
}

package org.mll.learning.storage.dubbo.provider;

import org.apache.dubbo.config.annotation.DubboService;
import org.mll.learning.dubbo.storage.DubboStorageService;
import org.mll.learning.storage.service.TccStorageService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class ProviderStorageServiceImpl implements DubboStorageService {

    @Autowired
    private TccStorageService tccStorageService;

    @Override
    public boolean deduct(Long productId, Integer count) {
        return tccStorageService.decrease(productId, count);
    }
}

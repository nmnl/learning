package org.mll.learning.storage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mll.learning.storage.entity.Storage;

@Mapper
public interface StorageMapper  {

    Storage selectById(@Param("productId") Long productId);

    void decrease(@Param("productId") Long productId, @Param("count")Integer count);

    void updateFrozen(@Param("productId") Long productId, @Param("residue") Integer residue, @Param("frozen") Integer frozen);

    void updateFrozenToUsed(@Param("productId") Long productId, @Param("count") Integer count);

    void updateFrozenToResidue(@Param("productId") Long productId, @Param("count") Integer count);
}

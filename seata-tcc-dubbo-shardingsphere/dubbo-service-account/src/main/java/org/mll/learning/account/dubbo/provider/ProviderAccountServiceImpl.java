package org.mll.learning.account.dubbo.provider;

import org.apache.dubbo.config.annotation.DubboService;
import org.mll.learning.account.service.TccAccountService;
import org.mll.learning.dubbo.account.DubboAccountService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@DubboService
public class ProviderAccountServiceImpl implements DubboAccountService {

    @Autowired
    private TccAccountService accountService;

    @Override
    public boolean decrease(Long userId, BigDecimal money) {
        return accountService.debit(null,userId, money);
    }
}

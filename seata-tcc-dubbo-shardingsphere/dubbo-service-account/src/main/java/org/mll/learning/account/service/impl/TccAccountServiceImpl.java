package org.mll.learning.account.service.impl;

import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.mll.learning.account.entity.Account;
import org.mll.learning.account.mapper.AccountMapper;
import org.mll.learning.account.service.TccAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Slf4j
@Service
public class TccAccountServiceImpl implements TccAccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean debit(BusinessActionContext businessActionContext, Long userId, BigDecimal money) {
        log.info("第一阶段XID:{} userId:{} money:{} ", RootContext.getXID(),userId,money);

        Account account = accountMapper.selectById(userId);
        if (account.getResidue().compareTo(money) < 0) {
            throw new RuntimeException("账户金额不足");
        }
        // 余额-money ; 冻结+money
        accountMapper.updateFrozen(userId, account.getResidue().subtract(money), account.getFrozen().add(money));
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        long userId = Long.parseLong(businessActionContext.getActionContext("userId").toString());
        BigDecimal money =  new BigDecimal(businessActionContext.getActionContext("money").toString());
        log.info("第二阶段提交XID:{} userId:{} money:{} ", RootContext.getXID(),userId,money);
        accountMapper.updateFrozenToUsed(userId, money);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        long userId = Long.parseLong(businessActionContext.getActionContext("userId").toString());
        BigDecimal money =  new BigDecimal(businessActionContext.getActionContext("money").toString());
        log.info("第二阶段回滚XID:{} userId:{} money:{} ", RootContext.getXID(),userId,money);
        accountMapper.updateFrozenToResidue(userId, money);
        // 这里如果是false,会一直被吊起;seata-server 一直发送
        return false;
    }
}

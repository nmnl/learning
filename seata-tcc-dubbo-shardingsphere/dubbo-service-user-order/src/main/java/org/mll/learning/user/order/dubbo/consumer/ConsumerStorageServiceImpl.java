package org.mll.learning.user.order.dubbo.consumer;

import org.apache.dubbo.config.annotation.DubboReference;
import org.mll.learning.dubbo.storage.DubboStorageService;
import org.springframework.stereotype.Component;

@Component
public class ConsumerStorageServiceImpl {

    @DubboReference(check = false)
    private DubboStorageService storageService;

    public boolean deduct(Long productId, Integer count){
        return storageService.deduct(productId,count);
    }
}

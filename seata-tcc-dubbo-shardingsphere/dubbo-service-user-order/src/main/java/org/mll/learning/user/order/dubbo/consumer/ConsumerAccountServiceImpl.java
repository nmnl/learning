package org.mll.learning.user.order.dubbo.consumer;

import org.apache.dubbo.config.annotation.DubboReference;
import org.mll.learning.dubbo.account.DubboAccountService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ConsumerAccountServiceImpl {

    @DubboReference(check = false)
    private DubboAccountService accountService;

    public boolean decrease(Long userId, BigDecimal money){
        return accountService.decrease(userId, money);
    }
}

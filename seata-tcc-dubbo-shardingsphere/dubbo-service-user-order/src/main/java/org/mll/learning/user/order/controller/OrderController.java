/*
 *  Copyright 1999-2021 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.mll.learning.user.order.controller;

import io.seata.spring.annotation.GlobalTransactional;
import org.mll.learning.user.order.dubbo.consumer.ConsumerAccountServiceImpl;
import org.mll.learning.user.order.dubbo.consumer.ConsumerStorageServiceImpl;
import org.mll.learning.user.order.entity.Order;
import org.mll.learning.user.order.service.OrderServiceTcc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private OrderServiceTcc orderServiceTcc;

    @Autowired
    private ConsumerStorageServiceImpl consumerStorageService;

    @Autowired
    private ConsumerAccountServiceImpl consumerAccountService;

    @PostMapping("/create/localTCC")
    @GlobalTransactional(rollbackFor = Exception.class)
    public Order createLocalhost(@RequestBody Order order) {
        // order tcc:创建订单
        orderServiceTcc.preCreateOrder(order,order.getId());
        // account tcc:修改账户余额 (用户ID / 金额)
        consumerAccountService.decrease(order.getUserId(), order.getMoney());
        // storage tcc:修改库存(产品id / 产品数量)
        consumerStorageService.deduct(order.getProductId(), order.getCount());
        return order;
    }
}

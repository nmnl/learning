## doc
```text
1. java启动参数
2. docker参数
```

## log_frame
```text
1.java日志框架对比,以及使用demo
2.demo:log4j/log4j2/logback
3.spring boot 默认集成的是logback
4.slf4j顶层接口门面 : org.slf4j.Logger
5.org.slf4j.LoggerFactory.getLogger(class);
```

## skywalking_frame
```text
1. skywalking-agent to java for plugin-log 实验插件
2. skywalking-oap-server for docker
3. skywalking-agent for docker
4. skywalking-oap-server for ha(nginx)
5. skywalking-agent to java for 链路 / 性能剖析 / 日志
```

## transaction_frame
* [atomikos](transaction_frame/README.md)

## tx-lcn
* [txlcn 搭建 LCN(Lock create/confirm notify)事务模式](tx-lcn_frame/README.md)

## hmily
* [hmily for feign tcc事务模式](hmily-demo/README.md)
* hmily for dubbo tcc事务模式

## seata
* [seata-tcc-mp-shardingsphere-druid](seata-tcc-mp-shardingsphere-druid/README.md)
* [seata-tcc-dubbo-shadingsphere](seata-tcc-dubbo-shadingsphere/README.md)
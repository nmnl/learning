# Java 测试环境启动参数
```text
java \
-server \
-Xdebug \
-Xnoagent \
-Djava.compiler=NONE \
-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=18688 \
-Xms500m -Xmx500m \
-Duser.timezone=Asia/Shanghai \
-Djava.awt.headless=true \
-Djava.security.egd=file:/dev/./urandom \
-Dfile.encoding=UTF8 \
-Dspring.cloud.nacos.config.password=nacos \
-Dspring.cloud.nacos.config.server-addr=192.168.10.143:8848 \
-jar /app.jar
```

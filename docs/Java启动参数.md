# java启动参数
```text
java命令启动应用所使用的参数，基本是用于JVM的，某种程度上也叫做JVM参数。总的来说，java启动参数共分为三大类，分别是：
标准参数(-)：相对稳定的参数，每个版本的JVM都可用。
非标准X参数(-X)：默认jvm实现这些参数的功能，但是并不保证所有jvm实现都满足，且不保证向后兼容。
XX参数(-XX)：此类参数各个jvm实现会有所不同，将来可能会随时取消。
```

## 常用标准参数
* -cp或-classpath <目录和 zip/jar 文件的类搜索路径>。使用-classpath后jvm将不再使用CLASSPATH中的类搜索路径，如果-classpath和CLASSPATH都没有设置，则jvm使用当前路径(.)作为类搜索路径。 `注意：linux用":",windows用";"来分隔目录, JAR 档案和 ZIP 档案列表, 用于搜索类文件。`
* -D<名称>=<值> 设置系统属性,运行在此jvm之上的应用程序可用System.getProperty(“property”)得到value的值。如果value中有空格，则需要用双引号将该值括起来，如-Dfoo=”foo bar”。该参数通常用于设置系统级全局变量值，如配置文件路径，以便该属性在程序中任何地方都可访问。如：maven跳过单元测试，使用java -Dmaven.test.skip=true
* -verbose:[class|gc|jni] 启用详细输出，一般在调试和诊断时，都会把gc的详细信息输出
* -version 输出产品版本并退出
* -? -help 输出此帮助消息
* -X 输出非标准选项的帮助
* -agentlib:<libname>[=<选项>] 加载本机代理库 <libname>, 例如 -agentlib:hprof 另请参阅 -agentlib:jdwp=help 和 -agentlib:hprof=help
* -agentpath:<pathname>[=<选项>] 按完整路径名加载本机代理库; -agentpath:/usr/local/agent 
* -javaagent:<jarpath>[=<选项>] 加载Java编程语言代理, 请参阅 java.lang.instrument
* -client 设置jvm使用client模式，特点是启动速度比较快，但运行时性能和内存管理效率不高，通常用于客户端应用程序或者PC应用开发和调试。
* -server 设置jvm使server模式，特点是启动速度比较慢，但运行时性能和内存管理效率很高，适用于生产环境。在具有64位能力的jdk环境下将默认启用该模式，而忽略-client参数。

## 常用X参数
* -Xbootclasspath: 设置搜索路径以引导类和资源，让jvm从指定路径（可以是分号分隔的目录、jar、或者zip）中加载bootclass，用来替换jdk的rt.jar
* -Xms 指定jvm堆的初始大小，默认为物理内存的1/64，最小为1M；可以指定单位，比如k、m，若不指定，则默认为字节。
* -Xmx 指定jvm堆的最大值，默认为物理内存的1/4或者1G，最小为2M；单位与-Xms一致。
* -Xss 设置单个线程栈的大小，一般默认为512k。
* -Xmn 设置堆(heap)的年轻代的初始值及最大值，单位与-Xms一致，年轻代是存储新对象的地址，也是GC发生得最频繁的地方，若设置过小，则会容易触发年轻代垃圾回收（minor gc），若设置过大，只触发full gc，则占用时间会很长，oracle建议是把年轻代设置在堆大小的四份之一到一半的。这命令同时设置了初始值和最大值，可以使用-XX:NewSize和-XX:MaxNewSiz来分别设置。

## 常用XX参数
* -XX:+PrintHeapAtGC
* -XX:+PrintGCDetails
* -XX:+PrintGCDateStamps
* -XX:+PrintGCTimeStamps
* -XX:+PrintTenuringDistribution
* -XX:+PrintGCApplicationStoppedTime
* -XX:+HeapDumpOnOutOfMemoryError
* -XX:+PrintFlagsFinal 输出参数的最终值
* -XX:+PrintFlagsInitial 输出参数的默认值
* -XX:-TraceClassLoading 跟踪类的加载信息
* -XX:-TraceClassLoadingPreorder 跟踪被引用到的所有类的加载信息
* -XX:-TraceClassResolution 跟踪常量池
* -XX:-TraceClassUnloading 跟踪类的卸载信息
* -XX:HeapDumpPath=logs/heapdump.hprof，发生OOM时，dump出快照到文件heapdump.hprof中。
* -XX:ErrorFile=logs/java_error_%p.log，发生JVM错误时，把日志输出到java_error_%p.log中。

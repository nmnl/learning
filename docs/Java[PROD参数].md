# Java生产启动参数

```text
java \
-server \
-javaagent:/skywalking-agent/skywalking-agent.jar \
-DSW_AGENT_NAME=customer-master -DSW_AGENT_COLLECTOR_BACKEND_SERVICES=ip:port \
-DSW_AGENT_AUTHENTICATION=AUTHENTICATION \
-Xms500m -Xmx500m \
-Duser.timezone=Asia/Shanghai
-Djava.awt.headless=true \
-Djava.security.egd=file:/dev/./urandom \
-Dfile.encoding=UTF8 \ 
-jar /app.jar
```

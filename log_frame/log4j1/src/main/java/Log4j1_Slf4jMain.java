import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log4j1_Slf4jMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(Log4j1_Slf4jMain.class);

    /**
     * <groupId>org.slf4j</groupId>
     * <artifactId>slf4j-log4j12</artifactId>
     * 实现了slf4j并可以使用
     * */
    public static void main(String[] args) {
        LOGGER.info(" info ");
        LOGGER.error(" error ");
        LOGGER.warn(" warn ");
    }
}

import org.apache.log4j.Logger;

public class Log4j1Main {

    private static final Logger LOGGER = Logger.getLogger(Log4j1Main.class);

    public static void main(String[] args) {
        LOGGER.info(" info ");
        LOGGER.error(" error ");
        LOGGER.warn(" warn ");
    }
}

import org.junit.jupiter.api.Test;


public class Logback2Test {

    @Test
    public void logback (){
        org.slf4j.Logger LOGGER_LOGBACK = org.slf4j.LoggerFactory.getLogger(Logback2Test.class);
        LOGGER_LOGBACK.info(" info ");
        LOGGER_LOGBACK.error(" error ");
        LOGGER_LOGBACK.warn(" warn ");
    }
}

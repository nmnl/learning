import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4j2Main {
    private static final Logger LOGGER = LogManager.getLogger(Log4j2Main.class);
    public static void main(String[] args) {
        LOGGER.info(" info ");
        LOGGER.error(" error ");
        LOGGER.warn(" warn ");
    }
}

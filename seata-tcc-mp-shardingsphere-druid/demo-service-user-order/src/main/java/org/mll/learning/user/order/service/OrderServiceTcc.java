package org.mll.learning.user.order.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;
import org.mll.learning.user.order.entity.Order;

@LocalTCC
public interface OrderServiceTcc {

    @TwoPhaseBusinessAction(name = "preCreateOrder", commitMethod = "commit", rollbackMethod = "cancel")
    boolean preCreateOrder(Order order, @BusinessActionContextParameter(paramName = "orderId") Long orderId) ;

    boolean commit(BusinessActionContext businessActionContext) ;

    boolean cancel(BusinessActionContext businessActionContext) ;
}

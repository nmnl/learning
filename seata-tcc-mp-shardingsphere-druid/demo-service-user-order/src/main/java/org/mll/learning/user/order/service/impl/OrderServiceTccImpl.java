package org.mll.learning.user.order.service.impl;

import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.mll.learning.user.order.entity.Order;
import org.mll.learning.user.order.mapper.OrderMapper;
import org.mll.learning.user.order.service.OrderServiceTcc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class OrderServiceTccImpl implements OrderServiceTcc {

    @Autowired
    private OrderMapper orderMapper;

    @Transactional(rollbackFor = Exception.class)
    public boolean preCreateOrder(Order order,Long orderId) {
        log.info("第一阶段XID:{} orderId:{}" , RootContext.getXID(),orderId);
        orderMapper.insert(order);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean commit(BusinessActionContext businessActionContext) {
        long orderId = Long.parseLong(businessActionContext.getActionContext("orderId").toString());
        log.info("第二阶段提交XID:{} orderId:{}" , RootContext.getXID(),orderId);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean cancel(BusinessActionContext businessActionContext) {
        long orderId = Long.parseLong(businessActionContext.getActionContext("orderId").toString());
        log.info("第二阶段回滚XID:{} orderId:{}" , RootContext.getXID(),orderId);
        return orderMapper.delete(orderId) > 0;
    }
}

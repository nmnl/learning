package org.mll.learning.user.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(name = "service-storage")
public interface StorageFeign {

    @PutMapping("/storage/{productId}/{count}")
    void deduct(@PathVariable("productId") Long productId, @PathVariable("count") Integer count);
}
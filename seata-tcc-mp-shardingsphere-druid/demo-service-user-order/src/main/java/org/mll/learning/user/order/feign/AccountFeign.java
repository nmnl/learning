package org.mll.learning.user.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient(name = "service-account")
public interface AccountFeign {

    @GetMapping("/decrease/{userId}/{money}")
    String decrease(@PathVariable("userId") Long userId, @PathVariable("money") BigDecimal money);
}

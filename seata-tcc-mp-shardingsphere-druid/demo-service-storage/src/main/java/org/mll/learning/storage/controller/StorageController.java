package org.mll.learning.storage.controller;

import org.mll.learning.storage.service.TccStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageController {

    @Autowired
    private TccStorageService tccStorageService;

    @PutMapping("/storage/{productId}/{count}")
    public ResponseEntity<Void> deduct(@PathVariable("productId") Long productId,@PathVariable("count") Integer count){
        tccStorageService.decrease(productId, count);
        return ResponseEntity.noContent().build();
    }
}

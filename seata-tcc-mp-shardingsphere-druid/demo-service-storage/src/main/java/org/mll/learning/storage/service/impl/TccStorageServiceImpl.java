package org.mll.learning.storage.service.impl;

import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.mll.learning.storage.entity.Storage;
import org.mll.learning.storage.mapper.StorageMapper;
import org.mll.learning.storage.service.TccStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class TccStorageServiceImpl implements TccStorageService {

    @Autowired
    private StorageMapper storageMapper;

    @Transactional
    @Override
    public boolean decrease(Long productId, Integer count) {
        log.info("第一阶段XID:{} productId:{} count:{} ", RootContext.getXID(),productId,count);
        Storage storage = storageMapper.selectById(productId);
        if (storage.getResidue()-count<0) {
            throw new RuntimeException("库存不足");
        }
        // 库存减掉count， 冻结库存增加count
        storageMapper.updateFrozen(productId, storage.getResidue()-count, storage.getFrozen()+count);
        // 本地标志
        log.info("扣减库存成功");
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        int count = Integer.parseInt(businessActionContext.getActionContext("count").toString());

        log.info("第二阶段提交XID:{} productId:{} count:{} ", RootContext.getXID(),productId,count);
        storageMapper.updateFrozenToUsed(productId, count);

        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean cancel(BusinessActionContext businessActionContext) {
        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        int count = Integer.parseInt(businessActionContext.getActionContext("count").toString());

        log.info("第二阶段回滚XID:{} productId:{} count:{} ", RootContext.getXID(),productId,count);
        storageMapper.updateFrozenToResidue(productId, count);

        // TODO 这里会回滚异常.
        return true;
    }
}

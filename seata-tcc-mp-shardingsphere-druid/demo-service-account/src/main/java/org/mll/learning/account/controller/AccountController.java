package org.mll.learning.account.controller;

import org.mll.learning.account.service.TccAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class AccountController {

    @Autowired
    private TccAccountService accountService;

    @GetMapping("/decrease/{userId}/{money}")
    public ResponseEntity<Void> debit(@PathVariable("userId") Long userId, @PathVariable("money") BigDecimal money){
        accountService.debit(null,userId, money);
        return ResponseEntity.noContent().build();
    }
}

package org.mll.learning.service.impl;

import org.mll.learning.entity.TestDO;
import org.mll.learning.mapper.finance.FinanceTestMapper;
import org.mll.learning.mapper.trade.TradeTestMapper;
import org.mll.learning.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private FinanceTestMapper financeTestMapper;

    @Autowired
    private TradeTestMapper tradeTestMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer save() {
        TestDO tradeTestDO = new TestDO();
        tradeTestDO.setDeleted(false);
        tradeTestDO.setInstId("1688");
        tradeTestDO.setGroupId("1");
        tradeTestMapper.insert(tradeTestDO);
        TestDO financeTestDO = new TestDO();
        financeTestDO.setDeleted(true);
        financeTestDO.setInstId("1688");
        financeTestDO.setGroupId("1");
        financeTestMapper.insert(financeTestDO);
        int i =1/0;
        return 0;
    }
}

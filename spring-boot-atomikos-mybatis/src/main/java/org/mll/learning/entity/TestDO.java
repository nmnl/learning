package org.mll.learning.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/*
 * @author mll
 * @version $Id: TestDTO.java, v 0.1 2022/4/18 mll $
 */
@Data
public class TestDO implements Serializable {

    private static final long serialVersionUID = 2382724780461633937L;

    private Long id;

    private boolean deleted;

    /** 创建人id */
    private String creatorId;

    /** 数据库时间*/
    private Date createDatetime;

    /** 修改人id */
    private String modifierId;

    /** 数据库时间 */
    private Date modifyDatetime;

    /** id */
    private String instId;

    /** id */
    private String groupId;

}

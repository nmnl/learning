package org.mll.learning.mapper.trade;

import org.apache.ibatis.annotations.Mapper;
import org.mll.learning.entity.TestDO;

@Mapper
public interface TradeTestMapper {

    Integer insert(final TestDO testDO);
}

package org.mll.learning.mapper.finance;

import org.apache.ibatis.annotations.Mapper;
import org.mll.learning.entity.TestDO;

@Mapper
public interface FinanceTestMapper {

    Integer insert(final TestDO testDO);
}

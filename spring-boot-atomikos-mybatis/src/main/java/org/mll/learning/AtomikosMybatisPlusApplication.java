package org.mll.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.TimeZone;

/**
 *
 * 应用启动类
 * @author mll
 * @version 1.0
 */
@SpringBootApplication
@EnableTransactionManagement
public class AtomikosMybatisPlusApplication {
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
        SpringApplication.run(AtomikosMybatisPlusApplication.class, args);
    }

}

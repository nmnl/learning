package org.mll.learning.controller;

import org.mll.learning.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
 * @author mll
 * @version $Id: TradeController.java, v 0.1 2022/4/15 mll $
 */
@RestController(value = "test/")
public class TestController {

    @Autowired
    private TestService testService;

    @PostMapping(value = "save")
    @ResponseBody
    public Integer save() {
        return testService.save();
    }

}

/* * Innotek-co.com Inc.
 * Copyright (c) 2010-2066 All Rights Reserved.
 */

package org.mll.learning.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mll.learning.util.DataSourceConfigUtil;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.sql.SQLException;

/*
 * @author mll
 * @version $Id: MybatisConfigFinance.java, v 0.1 2022/4/14 15:30 mll $
 */
@Configuration
@MapperScan(basePackages = { "org.mll.learning.mapper.finance" }, sqlSessionTemplateRef = "financeSqlSessionTemplate")
public class MybatisConfigFinance {

    /**
     * DataSource
     * 业务库(订单数据库):defTradeDataSource
     * */
    @Bean(name = "financeDataSource")
    public DataSource financeDataSource(Environment env) throws SQLException {
        return DataSourceConfigUtil.setMysqlXADataSource("spring.datasource.finance01.", "financeDataSource", env);
    }

    @Bean(name = "financeSqlSessionFactory")
    public SqlSessionFactory financeSqlSessionFactory(@Qualifier("financeDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource);
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().
                getResources("classpath*:mapper/finance/*.xml"));
        return sqlSessionFactory.getObject();
    }

    @Bean(name = "financeSqlSessionTemplate")
    public SqlSessionTemplate financeSqlSessionTemplate(@Qualifier("financeSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}

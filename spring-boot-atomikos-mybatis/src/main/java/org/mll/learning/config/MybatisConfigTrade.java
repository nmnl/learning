/*
 * Innotek-co.com Inc.
 * Copyright (c) 2010-2066 All Rights Reserved.
 */
package org.mll.learning.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mll.learning.util.DataSourceConfigUtil;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 *
 * trade分库mapper
 * @author mll
 * @version $Id: MybatisConfigTrade.java, v 0.1 2022/4/14 15:30 mll $
 */
@Configuration
@MapperScan(basePackages = { "org.mll.learning.mapper.trade" }, sqlSessionTemplateRef = "tradeSqlSessionTemplate")
public class MybatisConfigTrade {

    /**
     * DataSource
     * 业务库(订单数据库):tradeDataSource
     * */
    @Bean(name = "tradeDataSource")
    public DataSource tradeDataSource(Environment env) throws SQLException {
        return DataSourceConfigUtil.setMysqlXADataSource("spring.datasource.trade01.", "tradeDataSource", env);
    }

    @Bean(name = "tradeSqlSessionFactory")
    public SqlSessionFactory tradeSqlSessionFactory(@Qualifier("tradeDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource);
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().
                getResources("classpath*:mapper/trade/*.xml"));
        return sqlSessionFactory.getObject();
    }

    @Bean(name = "tradeSqlSessionTemplate")
    public SqlSessionTemplate tradeSqlSessionTemplate(@Qualifier("tradeSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

}

package org.mll.learning.util;

import com.mysql.cj.jdbc.MysqlXADataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.SQLException;

public class DataSourceConfigUtil {

    public static DataSource setMysqlXADataSource(String prefix1, String uniqueResourceName, Environment env) throws SQLException {
        AtomikosDataSourceBean sourceBean = new AtomikosDataSourceBean();
        sourceBean.setUniqueResourceName(uniqueResourceName);
        sourceBean.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
        sourceBean.setTestQuery("select 1");
        sourceBean.setBorrowConnectionTimeout(3);

        MysqlXADataSource dataSource = new MysqlXADataSource();
        dataSource.setPinGlobalTxToPhysicalConnection(true);
        String dataSourceUrl = env.getProperty(prefix1 + "url");
        String user = env.getProperty(prefix1 + "username");
        String password = env.getProperty(prefix1 + "password");
        if (StringUtils.isNotBlank(dataSourceUrl)) {
            dataSource.setUrl(dataSourceUrl);
        }
        if (StringUtils.isNotBlank(user)) {
            dataSource.setUser(user);
        }
        if (StringUtils.isNotBlank(password)) {
            dataSource.setPassword(password);
        }
        sourceBean.setXaDataSource(dataSource);
        return sourceBean;
    }


}

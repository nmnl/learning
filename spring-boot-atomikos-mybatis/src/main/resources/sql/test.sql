CREATE TABLE `finance_test` (
                                `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除:0-否、1-是',
                                `creator_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '记录创建人',
                                `create_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '数据库记录创建时间',
                                `modifier_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '记录修改者',
                                `modify_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '数据库记录修改时间',
                                `inst_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
                                `group_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='test临时表';
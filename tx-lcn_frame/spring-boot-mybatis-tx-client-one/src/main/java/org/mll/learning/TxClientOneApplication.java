package org.mll.learning;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 应用启动类;开启分布式事务EnableDistributedTransaction
 * @author mll
 * @version 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableDistributedTransaction
@EnableFeignClients
public class TxClientOneApplication {
    public static void main(String[] args) {
        SpringApplication.run(TxClientOneApplication.class, args);
    }
}

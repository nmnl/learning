package org.mll.learning.controller;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.mll.learning.entity.UserDO;
import org.mll.learning.feign.TwoServiceFeign;
import org.mll.learning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 * @author mll
 * @version $Id: UserController.java, v 0.1 2022/4/18 mll $
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TwoServiceFeign twoServiceFeign;


    /**
     * 测试分布式事务
     * 本地事务保存;这里抛出异常属于本地事务需要加@Transactional注解
     * 远程事务保存;此方法若抛出异常，使用@LcnTransaction注解就可以使上面数据回滚
     * @param flag 要添加的数据
     */
    @PostMapping("/user/save")
    @Transactional
    @LcnTransaction
    void save(@RequestParam(required = false, name = "flag") boolean flag) {
        UserDO userDO = new UserDO();
        userService.save(userDO);
        twoServiceFeign.save(flag,userDO.getId());
    }

}

package org.mll.learning.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/*
 * @author mll
 * @version $Id: TwoServiceFeign.java, v 0.1 2022/4/18 mll $
 */
@FeignClient("tx-client-two")
public interface TwoServiceFeign {

    @PostMapping("/message/save")
    void save(@RequestParam(required = false, name = "flag") boolean flag,@RequestParam(name = "userId") Long userId);

}

package org.mll.learning.service.impl;

import org.mll.learning.entity.UserDO;
import org.mll.learning.mapper.UserMapper;
import org.mll.learning.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/*
 * @author mll
 * @version $Id: UserServiceImpl.java, v 0.1 2022/4/18 mll $
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public void save(UserDO userDO) {
        userMapper.save(userDO);
    }
}

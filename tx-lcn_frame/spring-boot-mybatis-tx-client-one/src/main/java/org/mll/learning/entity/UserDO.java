package org.mll.learning.entity;

import java.io.Serializable;
import java.util.Date;

/*
 * @author mll
 * @version $Id: UserDO.java, v 0.1 2022/4/18 mll $
 */
public class UserDO implements Serializable {
    private static final long serialVersionUID = 695700593668520206L;

    private Long id;

    private String name;

    /** 创建人id */
    private Integer age;

    /** 数据库时间*/
    private Date createDatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }
}

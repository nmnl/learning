package org.mll.learning.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.mll.learning.entity.UserDO;

/*
 * @author mll
 * @version $Id: UserMapper.java, v 0.1 2022/4/18 mll $
 */
@Mapper
public interface UserMapper {

    /**
     * 保存
     *
     * @param userDO 用户信息
     */
    @Insert("insert into user(id, name, age, create_datetime) values(#{id}, #{name }, #{age}, #{createDatetime})")
    Integer save(UserDO userDO);

}

package org.mll.learning.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.mll.learning.entity.MessageDO;

/*
 * @author mll
 * @version $Id: MessageMapper.java, v 0.1 2022/4/18 mll $
 */
@Mapper
public interface MessageMapper {

    /**
     * 保存
     *
     * @param messageDO
     */
    @Insert("insert into message(id, user_id, title, content, create_datetime) values(#{id}, #{userId }, #{title},#{content}, #{createDatetime})")
    Integer save(MessageDO messageDO);

}

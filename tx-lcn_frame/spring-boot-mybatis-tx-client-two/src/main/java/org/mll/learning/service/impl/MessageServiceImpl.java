package org.mll.learning.service.impl;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.mll.learning.entity.MessageDO;
import org.mll.learning.mapper.MessageMapper;
import org.mll.learning.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/*
 * @author mll
 * @version $Id: MessageService.java, v 0.1 2022/4/18 mll $
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageMapper messageMapper;

    /**
     * 测试分布式事务
     * 本地事务保存;这里抛出异常属于本地事务需要加@Transactional注解
     * 远程事务保存;此方法若抛出异常，使用@LcnTransaction注解就可以使上面数据回滚
     * @param messageDO 要添加的数据
     */
    @Transactional
    @LcnTransaction
    @Override
    public void save(MessageDO messageDO) {
        if (messageDO.getTitle().equals("异常")){
            throw new IllegalStateException("执行出错,抛出异常");
        }
        messageMapper.save(messageDO);
    }
}

package org.mll.learning.controller;

import org.mll.learning.entity.MessageDO;
import org.mll.learning.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/*
 * @author mll
 * @version $Id: MessageService.java, v 0.1 2022/4/18 mll $
 */
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/message/save")
    private void save(@RequestParam(required = false, name = "flag") boolean flag,@RequestParam(name = "userId") Long userId) {
        MessageDO messageDO = new MessageDO();
        messageDO.setUserId(userId);
        messageDO.setTitle("title" + System.currentTimeMillis());
        if (flag) {
            messageDO.setTitle("异常");
        }
        messageDO.setContent("title" + System.currentTimeMillis());
        messageDO.setCreateDatetime(new Date());
        messageService.save(messageDO);
    }

}

package org.mll.learning.entity;

import java.io.Serializable;
import java.util.Date;

/*
 * @author mll
 * @version $Id: MessageDO.java, v 0.1 2022/4/18 mll $
 */
public class MessageDO implements Serializable {

    private static final long serialVersionUID = 4240940624275932927L;

    private Long id;

    private Long userId;

    private String title;

    /** 内容 */
    private String content;

    /** 数据库时间*/
    private Date createDatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }
}

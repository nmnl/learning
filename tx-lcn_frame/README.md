* tx-lcn_frame(个人学习)
```text
本文演示LCN事务模式, for spring cloud
* txlcn-tm 事务控制端
1. 依赖mysql / redis
2. tm不能使用yml配置.
* txlcn-tc(client):技术栈
1. nacos(无限制)
2. redis(无限制)
3. mysql 8
4. feign
5. spring-boot 2.1.6
```
* [代码](https://github.com/codingapi/tx-lcn.git)
* [文档](https://www.codingapi.com/docs/txlcn-debug/)
* [TCC事务模式](ZTransactionMode/TCC模式.md)
* [TXC事务模式](ZTransactionMode/TXC模式.md)
* [LCN事务模式](ZTransactionMode/LCN模式.md)

* 代码整体架构(很遗憾,已经很久未更新了.)
```text
example: 示例与测试相关的代码
starter-txlcn-protocol: txlcn-protocol模块的starter
starter-txlcn-tc: txlcn-tc模块的starter
txlcn-p6spy: p6spy-解析sql与jdbc的event定义
txlcn-protocol: 通讯协议制度
txlcn-tc: TC事务客户端模块
txlcn-tm: TM事务控制端
```

* 原理图
![img.png](ZImg/img.png)

* 核心步骤
```text
1. 创建事务组:是指在事务发起方开始执行业务代码之前先调用TxManager创建事务组对象，然后拿到事务标示GroupId的过程。
2. 加入事务组:添加事务组是指参与方在执行完业务方法以后，将该模块的事务信息通知给TxManager的操作。
3. 通知事务组:是指在发起方执行完业务代码以后，将发起方执行结果状态通知给TxManager,TxManager将根据事务最终状态和事务组的信息来通知相应的参与模块提交或回滚事务，并返回结果给事务发起方。
```
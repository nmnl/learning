CREATE TABLE `t_tx_exception` (
                                  `id` bigint NOT NULL AUTO_INCREMENT,
                                  `group_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                  `unit_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                  `mod_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                                  `transaction_state` tinyint DEFAULT NULL,
                                  `registrar` tinyint DEFAULT NULL,
                                  `ex_state` tinyint DEFAULT NULL COMMENT '0 待处理 1已处理',
                                  `remark` varchar(10240) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;